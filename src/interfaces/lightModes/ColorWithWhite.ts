export interface ColorWithWhite {
  red: number;
  green: number;
  blue: number;
  ww: number;
  alpha: number;
}

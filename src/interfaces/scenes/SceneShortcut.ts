export interface SceneShortcut {
  id: string;
  name: string;
  sceneIds: string[];
}

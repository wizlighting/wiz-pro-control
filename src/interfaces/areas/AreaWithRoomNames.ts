export interface AreaWithRoomNames {
  id: string;
  name: string;
  rooms: {
    id: string;
    name: string;
  }[];
}

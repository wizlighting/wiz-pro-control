export interface LightWithCurrentState {
  id: string;
  name: string;
  currentState: {
    state: boolean;
    dimming?: number;
    speed?: number;
    ratio?: number;
    temperature?: number;
    r?: number;
    g?: number;
    b?: number;
    cw?: number;
    ww?: number;
    sceneId?: number;
  };
}

import { LightWithCurrentState } from "@/interfaces/lights/LightWithCurrentState";

export interface GroupWithLights {
  id: string;
  name: string;
  lights: LightWithCurrentState[];
}

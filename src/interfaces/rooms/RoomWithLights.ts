import { LightWithCurrentState } from "@/interfaces/lights/LightWithCurrentState";
import { GroupWithLights } from "@/interfaces/groups/GroupWithLights";
import { FavoriteStateFields } from "@/graphqlRequests/fragments/FavoriteStateFragment";

export interface RoomWithLights {
  id: string;
  name: string;
  favorite1: FavoriteStateFields.Definition;
  favorite2: FavoriteStateFields.Definition;
  favorite3: FavoriteStateFields.Definition;
  favorite4: FavoriteStateFields.Definition;
  lights: LightWithCurrentState[];
  groups: GroupWithLights[];
}

export interface RoomScene {
  id: string;
  name: string;
}

export interface RoomWithScenes {
  id: string;
  name: string;
  roomScenes: RoomScene[];
}

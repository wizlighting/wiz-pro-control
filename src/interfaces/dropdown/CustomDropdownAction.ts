export interface CustomDropdownAction {
  key: string;
  label: string;
}

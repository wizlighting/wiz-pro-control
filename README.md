# wiz-pro-control

## Project setup
### Set the environment variables
```
cp env-example .env

nano .env 
# Update the value in the .env 
```

### Install the dependencies
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## FAQ
## Where can I find the API documentation
Please refer to https://docs.pro.wizconnected.com/#introduction

## What should I put in the `VUE_APP_WIZ_PRO_CLIENT_ID` in .env
Please contact WiZ Pro Dashboard Support for further information

https://wizconnected.helpshift.com/hc/en/5-wiz-pro-dashboard/

